import { useRouter } from 'next/router'
import { Container, Row, Col } from 'react-bootstrap';

export default function Products() {
    const router = useRouter();
    const { type } = router.query;
    const Filter = () => (
        <Row>
            <Col className="d-lg-none">
                <h1>X</h1>
            </Col>
        </Row>
    );

  return (
    <Container fluid>
        <Filter />
        <Row>
            <Col lg={3} className="d-none d-lg-block">
                <h1>Filter</h1>
            </Col>
            <Col lg={9}>
                <h1>{ type }</h1>
            </Col>
        </Row>
    </Container>
  );
}