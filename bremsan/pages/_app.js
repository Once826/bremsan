import { Container, Link, NextUIProvider, Row } from '@nextui-org/react';
import { Avatar} from "@nextui-org/react";
import styles from '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import SSRProvider from 'react-bootstrap/SSRProvider';

function MyApp({ Component, pageProps }) {
  const NavBar = () => (
    <Navbar id="navbar" expand="md" collapseOnSelect bg="warning">
      <Navbar.Brand href="/">Brem San</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
      <Nav>
        <Nav.Link href="/" eventKey="home">Home</Nav.Link>
        <NavDropdown title="Products" id="basic-nav-dropdown">
          <NavDropdown.Item href="/products">All</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/products/concentrates">Concentrates</NavDropdown.Item>
          <NavDropdown.Item href="/products/premixes">Premixes</NavDropdown.Item>
          <NavDropdown.Item href="/products/cereals">Cereals</NavDropdown.Item>
          <NavDropdown.Item href="/products/others">Others</NavDropdown.Item>
        </NavDropdown>
        <Nav.Link eventKey="Contacts" href='/contacts'>Contacts</Nav.Link>
      </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
  return  (
    <SSRProvider>
      <NavBar />
      <Component {...pageProps} />
    </SSRProvider>
  );
}

export default MyApp
